import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

export const routes = [
    {
        path: "/",
        name: "Dashboard",
        component: Home,
        icon: "mdi-view-dashboard",
        nav: true,
    },
    {
        path: "/transactions",
        name: "Transactions",
        component: () => import(/* webpackChunkName: "about" */ "../views/Transaction.vue"),
        icon: "mdi-swap-horizontal",
        nav: true,
    },
    {
        path: "/budget",
        name: "Budget",
        component: () => import(/* webpackChunkName: "about" */ "../views/Budget.vue"),
        icon: "mdi-cash-100",
        nav: true,
    },
    {
        name: "AccountID",
        path: "/account/:id",
        component: () => import(/* webpackChunkName: "account" */ "../views/Account.vue"),
        nav: false,
    },
    {
        name: "Account",
        path: "/account",
        component: () => import(/* webpackChunkName: "account" */ "../views/Account.vue"),
        nav: false,
    },
    {
        name: "JobID",
        path: "/job/:id",
        component: () => import(/* webpackChunkName: "about" */ "../views/Transaction.vue"),
        nav: false,
    },
    {
        name: "Job",
        path: "/job",
        component: () => import(/* webpackChunkName: "about" */ "../views/Transaction.vue"),
        nav: false,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
