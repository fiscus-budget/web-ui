import Vue from "vue";
import Vuex from "vuex";
// Import store modules
import Config from "./modules/config";
import Budget from "./modules/budget";
import Account from "./modules/account";

Vue.use(Vuex);
/*
export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {},
});
*/

export default new Vuex.Store({
    modules: {
        config: Config,
        budget: Budget,
        account: Account,
    },
});
