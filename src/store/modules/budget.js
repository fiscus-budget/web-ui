const Store = {
    state: () => ({
        budgetID: "",
        budgetName: "",
        budget: {},
        budgets: [],
    }),
    mutations: {
        UPDATE_BUDGETID(state, payload) {
            state.budgetID = payload;
        },
        UPDATE_BUDGETNAME(state, payload) {
            state.budgetName = payload;
        },
        UPDATE_BUDGET(state, payload) {
            state.budget = payload;
        },
        UPDATE_BUDGETS(state, payload) {
            state.budgets = payload;
        },
    },
    actions: {
        insertBudget({ commit }, payload) {
            commit("UPDATE_BUDGET", payload);
            commit("UPDATE_BUDGETID", payload.id);
            commit("UPDATE_BUDGETNAME", payload.name);
        },
        insertBudgets({ commit }, payload) {
            commit("UPDATE_BUDGETS", payload);
        },
        deleteBudget({ commit }) {
            commit("UPDATE_BUDGETID", "");
            commit("UPDATE_BUDGETNAME", "");
            commit("UPDATE_BUDGET", {});
        },
    },
    getters: {
        budgetID(state) {
            return state.budgetID;
        },
    },
};

export default Store;

export const mutations = Store.mutations;
export const getters = Store.getters;
export const actions = Store.actions;
