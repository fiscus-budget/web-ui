const Store = {
    state: () => ({
        accounts: [],
    }),
    mutations: {
        UPDATE_ACCOUNTS(state, payload) {
            state.accounts = payload;
        },
        UPDATE_BUDGETNAME(state, payload) {
            state.budgetName = payload;
        },
    },
    actions: {
        insertAccount({ commit, state }, payload) {
            let accounts = state.accounts.concat(payload);
            commit("UPDATE_ACCOUNTS", accounts);
        },
        insertAccounts({ commit }, payload) {
            if (Array.isArray(payload)) {
                commit("UPDATE_ACCOUNTS", payload);
            }
        },
        deleteAccounts({ commit }) {
            commit("UPDATE_ACCOUNTS", []);
        },
    },
    getters: {
        accounts(state) {
            return state.accounts;
        },
    },
};

export default Store;

export const mutations = Store.mutations;
export const getters = Store.getters;
export const actions = Store.actions;
