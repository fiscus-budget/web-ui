const Store = {
    state: () => ({
        backendAPI: "",
    }),
    mutations: {
        INSERT_BACKENDAPI(state, payload) {
            state.backendAPI = payload;
        },
    },
    actions: {
        insertConfig({ commit }, payload) {
            commit("INSERT_BACKENDAPI", payload.backendAPI);
        },
    },
    getters: {
        backendAPI(state) {
            return state.backendAPI;
        },
    },
};

export default Store;

export const mutations = Store.mutations;
export const getters = Store.getters;
export const actions = Store.actions;
