import axios from "axios";
import store from "../store";

// TODO: Implement error handling
class AccountService {
    // We dont return anything here....
    async getAll() {
        try {
            const response = await axios.get(
                `${store.getters.backendAPI}/api/labels?budgetID=${store.getters.budgetID}&limit=10000`,
            );
        } catch (error) {
            console.error(error);
        }
    }
    // Return Account
    async get(id) {
        return axios.get(`${store.getters.backendAPI}/api/account/${id}`);
    }

    create(data) {
        return axios.post(`${store.getters.backendAPI}/api/account`, data);
    }

    update(id, data) {
        return axios.put(`${store.getters.backendAPI}/api/account/${id}`, data);
    }

    delete(id) {
        return axios.delete(`${store.getters.backendAPI}/api/account/${id}`);
    }
}

export default new AccountService();
