import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: colors.teal.base,
                secondary: colors.lime.accent3,
                accent: colors.amber.accent4,
            },
        },
    },
});
