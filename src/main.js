import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";

Vue.config.productionTip = false;

// Get config
axios
    .get("/config.json")
    .then(function (response) {
        store.dispatch("insertConfig", response.data);
        new Vue({
            router,
            store,
            vuetify,
            render: h => h(App),
        }).$mount("#app");
    })
    .catch(function (error) {
        // Implement default API url...
        console.log(error);
    });
/*
new Vue({
    router,
    store,
    vuetify,
    render: h => h(App),
}).$mount("#app");
*/
