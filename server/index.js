//import { generateName, getRandomInt } from "./helpers";
var helper = require("./helpers");
var uuid = require("uuid");

module.exports = () => {
    // Data object
    const data = {
        budgets: [
            {
                id: uuid.v4(),
                name: "Personal",
            },
            {
                id: uuid.v4(),
                name: "Family",
            },
        ],
        accounts: [],
        jobs: [],
        transactions: [],
        labels: [],
        "job-labels": [],
        accounttypes: [
            {
                id: uuid.v4(),
                name: "Bank Account",
                created_at: "2022-01-09",
                updated_at: "2022-01-09",
            },
            {
                id: uuid.v4(),
                name: "Wallet",
                created_at: "2022-01-09",
                updated_at: "2022-01-09",
            },
            {
                id: uuid.v4(),
                name: "Credit card",
                created_at: "2022-01-09",
                updated_at: "2022-01-09",
            },
            {
                id: uuid.v4(),
                name: "Pension Fond",
                created_at: "2022-01-09",
                updated_at: "2022-01-09",
            },
        ],
        404: [],
    };
    // Generator Functions
    // Create accounts for budgets
    function generateBudgetAccounts(budget) {
        for (let i = 0; i < helper.getRandomInt(1, 4); i++) {
            let obj = {
                id: uuid.v4(),
                value: helper.getRandomInt(0, 50000),
                name: helper.generateName(),
                budgetID: budget.id,
                typeID: data.accounttypes[helper.getRandomInt(0, 3)].id,
            };
            data.accounts.push(obj);
        }
    }
    // Create labels for budgets
    function generateBudgetLabels(budget) {
        for (let i = 0; i < helper.getRandomInt(2, 10); i++) {
            let obj = {
                id: uuid.v4(),
                name: helper.generateName(),
                budgetID: budget.id,
            };
            data.labels.push(obj);
        }
    }
    // Generate Transactions

    // Generate Jobs
    function generateBudgetJobs(budget) {
        for (let i = 0; i < helper.getRandomInt(10, 35); i++) {
            let obj = {
                id: uuid.v4(),
                name: helper.generateName(),
                value: helper.getRandomInt(0, 50000),
                budgetID: budget.id,
            };
            data.jobs.push(obj);
        }
    }
    // Relate Jobs to Labels

    // factory calling all generator functions
    function factory(budget) {
        generateBudgetLabels(budget);
        generateBudgetAccounts(budget);
        generateBudgetJobs(budget);
    }

    data.budgets.forEach(factory);

    return data;
};

module.exports = () => {
    // Data object
    const data = {
        budgets: [
            {
                id: uuid.v4(),
                name: "Personal",
            },
            {
                id: uuid.v4(),
                name: "Family",
            },
        ],
        accounts: [],
        jobs: [],
        transactions: [],
        labels: [],
        "job-labels": [],
        accounttypes: [
            {
                id: uuid.v4(),
                name: "Bank Account",
                created_at: "2022-01-09",
                updated_at: "2022-01-09",
            },
            {
                id: uuid.v4(),
                name: "Wallet",
                created_at: "2022-01-09",
                updated_at: "2022-01-09",
            },
            {
                id: uuid.v4(),
                name: "Credit card",
                created_at: "2022-01-09",
                updated_at: "2022-01-09",
            },
            {
                id: uuid.v4(),
                name: "Pension Fond",
                created_at: "2022-01-09",
                updated_at: "2022-01-09",
            },
        ],
        404: [],
    };
    // Generator Functions
    // Create accounts for budgets
    function generateBudgetAccounts(budget) {
        for (let i = 0; i < helper.getRandomInt(1, 4); i++) {
            let obj = {
                id: uuid.v4(),
                value: helper.getRandomInt(0, 50000),
                name: helper.generateName(),
                budgetID: budget.id,
                typeID: data.accounttypes[helper.getRandomInt(0, 3)].id,
            };
            data.accounts.push(obj);
        }
    }
    // Create labels for budgets
    function generateBudgetLabels(budget) {
        for (let i = 0; i < helper.getRandomInt(2, 10); i++) {
            let obj = {
                id: uuid.v4(),
                name: helper.generateName(),
                budgetID: budget.id,
            };
            data.labels.push(obj);
        }
    }
    // Generate Transactions

    // Generate Jobs
    function generateBudgetJobs(budget) {
        for (let i = 0; i < helper.getRandomInt(10, 35); i++) {
            let obj = {
                id: uuid.v4(),
                name: helper.generateName(),
                value: helper.getRandomInt(0, 50000),
                budgetID: budget.id,
            };
            data.jobs.push(obj);
        }
    }
    // Relate Jobs to Labels

    // factory calling all generator functions
    function factory(budget) {
        generateBudgetLabels(budget);
        generateBudgetAccounts(budget);
        generateBudgetJobs(budget);
    }

    data.budgets.forEach(factory);

    return data;
};
