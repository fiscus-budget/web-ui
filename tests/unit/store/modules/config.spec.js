import { mutations } from "../../../../src/store/modules/config";

// destructure assign `mutations`
const { INSERT_BACKENDAPI } = mutations;

describe("mutations", () => {
    test("UPDATE_BACKENDAPI", () => {
        // mock state
        const state = { backendAPI: 0 };
        // apply mutation
        let url = "http://localhost:3000";
        INSERT_BACKENDAPI(state, "http://localhost:3000");
        // assert result
        expect(state.backendAPI).toEqual(url);
    });
});
